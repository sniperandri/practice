<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

route::resource('testings','TestingController');
Route::get('/crud/index', 'TestingController@index');
Route::get('/crud/create', 'TestingController@create');

route::resource('genders','GenderController');
Route::get('/crud/tambahan', 'GenderController@create');
Route::get('/crud/table', 'GenderController@index');

Route::get('/hello/{name}',[
	'uses'	=> 'TestController@hello',
	'as'	=> 'test.hello'
]);


