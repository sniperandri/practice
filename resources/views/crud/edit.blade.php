@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Form</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p><a href="/crud/index">Kembali</a></p>
                    
                    <div class="container">
                        <form method="post" action="{{action('TestingController@update', $id)}}" enctype="multipart/form-data">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="row">
                        <div class="col-md-12"></div>
                        <div class="form-group col-md-12">
                            <label for="Name">Nama</label>
                            <input type="text" class="form-control" name="name" value="{{$testing->name}}">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="Email">Email</label>
                            <input type="email" class="form-control" name="email" value="{{$testing->email}}">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="Umur">Umur</label>
                            <input type="text" class="form-control" name="umur" value="{{$testing->umur}}">
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12"></div>
                        <div class="form-group col-md-4" style="margin-top:10px">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
