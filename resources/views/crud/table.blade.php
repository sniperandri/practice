@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Table Gender</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <a href="/crud/index">kembali</a> |
                    <a href="/crud/tambahan">Data Tambahan</a>

                    <div class="container">
                        <br />
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div><br />
                        @endif
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Jenis Kelamin</th>
                            </tr>
                            </thead>
                            <tbody>
                        
                            @foreach($genders as $gender)
                            <tr>
                                <td>{{$gender['id']}}</td>
                                <td>{{$gender['gender']}}</td>

                                <td align="right"><a href="{{action('GenderController@edit', $gender['id'])}}" class="btn btn-warning">Edit</a></td>
                                <td align="left">
                                <form action="{{action('GenderController@destroy', $gender['id'])}}" method="post">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
