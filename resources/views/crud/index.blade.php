@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Table</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <a href="/crud/create">Tambah Data</a> |
                    <a href="/crud/table">Table Gender</a>

                    <div class="container">
                        <br />
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div><br />
                        @endif
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Umur</th>
                            </tr>
                            </thead>
                            <tbody>
                        
                            @foreach($testings as $testing)
                            <tr>
                                <td>{{$testing['id']}}</td>
                                <td>{{$testing['name']}}</td>
                                <td>{{$testing['email']}}</td>
                                <td>{{$testing['umur']}}</td>

                                <td align="right"><a href="{{action('TestingController@edit', $testing['id'])}}" class="btn btn-warning">Edit</a></td>
                                <td align="left">
                                <form action="{{action('TestingController@destroy', $testing['id'])}}" method="post">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
