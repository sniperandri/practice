@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Form</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p><a href="/crud/index">Kembali</a></p>
                    
                    <div class="container">
                        <form method="post" action="{{url('testings')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                            <div class="col-md-12"></div>
                            <div class="form-group col-md-12">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" name="name" placeholder="masukan nama">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="masukan email">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="umur">Umur</label>
                                <input type="number" class="form-control" name="umur" placeholder="">
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12"></div>
                            <div class="form-group col-md-4" style="margin-top:10px">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
