<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testing extends Model
{
    protected $fillable = ['name', 'email', 'umur', 'jenis_kelamin_id'];
}
