<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testing;

class TestingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testings = Testing::all();
        return view('/crud/index',compact('testings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/crud/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $testing = new Testing();
        $testing->name = $request->get('name');
        $testing->email = $request->get('email');
        $testing->umur = $request->get('umur');
        $testing->save();
     
        return redirect('testings')->with('success','testing has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testing = Testing::find($id);
        return view('/crud/edit',compact('testing','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testing= Testing::find($id);
        $testing->name = $request->get('name');
        $testing->email = $request->get('email');
        $testing->umur = $request->get('umur');
        $testing->save();
        return redirect('testings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testing = Testing::find($id);
        $testing->delete();
        return redirect('testings')->with('success','Testing Has Been Deleted');
    }
}
