<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function hello($name){
    	return view('hello',compact('name'));
    }
}
